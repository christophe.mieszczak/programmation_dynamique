#TP Programmation Dynamique COVID
# Mieszczak Christophe

# coding : utf-8

def lecture(nom_fichier):
    filin = open(nom_fichier, "r")
    entete=filin.readline()    
    lignes=[ s.rstrip() for s in filin.readlines()]
    filin.close()
    return lignes


def minimum(a,b,c):
    """
    renvoie le minimum des trois entiers a,b,c
    : param a,b,c
    type int
    : return
    int
    >>> minimum(1,2,3)
    1
    >>> minimum(2,1,3)
    1
    >>> minimum(2,3,1)
    1
    """
    return min(a, min(b, c))    

#############################################
########## Matrice de mémoïsation
############################################

def initialiser_matrice(seq1, seq2):
    '''
    renvoie une matrice d'édition initiale obtenue à parir des chaînes seq1 et seq2
    :param seq1, seq2
    type str
    : return la matrice
    type list of list
    >>> initialiser_matrice('ABCD','AECHJ') 
    [[0, 1, 2, 3, 4], [1, 0, 0, 0, 0], [2, 0, 0, 0, 0], [3, 0, 0, 0, 0], [4, 0, 0, 0, 0], [5, 0, 0, 0, 0]]
    '''
    matrice = [ [0] * (len(seq1) + 1) for j in range(len(seq2) + 1) ]
    for i in range(len(seq1) + 1) :
        matrice[0][i] = i
    for j in range(len(seq2) + 1):
        matrice[j][0] = j
    return matrice

def valeur(matrice, i, j):
    """
    renvoie matrice[j][i]
    : param matrice
    type list of list
    : param i, j deux entiers supérieurs ou égaux à -1
    type int
    return un element de la matrice 
    type int
    >>> A = [[1,2,3],[2,3,4]]
    >>> valeur(A,1,1) == 3
    True
    >>> valeur(A,-1,1) == 2
    True
    """
    assert(isinstance(i, int) and isinstance(j, int) and i>=-1 and j >= -1), 'indices hors domaine'
    if i >= 0 and j >= 0:
        return matrice[j][i]
    elif j == -1:
        return i + 1
    else :
        return j + 1

def un_ou_zero(seq1, seq2, i, j):
    '''
    renvoie 1 si seq[i] est différent de seq[j] et 0 sinon
    : param seq1, seq2
    type str
    : param i, j
    type int
    : return 1 ou 0
    type int
    '''
    if seq1[i] != seq2[j] :
        return 1
    else :
        return 0


def remplir_matrice(seq1, seq2, matrice):
    '''
    Renvoie la matrice d'édition complétée
    : param seq1, seq2 les 2 chaines
    type str
    : param matrice
    type list of list
    : EFFET DE BORD sur matrice
    : Pas de return
    >>> seq2 = 'ALGORITHME'
    >>> seq1 = 'AGORRYTNE'
    >>> matrice = initialiser_matrice(seq1, seq2) 
    >>> remplir_matrice(seq1, seq2, matrice)
    >>> matrice
    [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [1, 0, 1, 2, 3, 4, 5, 6, 7, 8], [2, 1, 1, 2, 3, 4, 5, 6, 7, 8], [3, 2, 1, 2, 3, 4, 5, 6, 7, 8], [4, 3, 2, 1, 2, 3, 4, 5, 6, 7], [5, 4, 3, 2, 1, 2, 3, 4, 5, 6], [6, 5, 4, 3, 2, 2, 3, 4, 5, 6], [7, 6, 5, 4, 3, 3, 3, 3, 4, 5], [8, 7, 6, 5, 4, 4, 4, 4, 4, 5], [9, 8, 7, 6, 5, 5, 5, 5, 5, 5], [10, 9, 8, 7, 6, 6, 6, 6, 6, 5]]
    '''
    for j in range(1, len(seq2) + 1):
        for i in range(1, len(seq1) + 1) :
            matrice[j][i] = minimum(valeur(matrice, i, j - 1) + 1,
                                    valeur(matrice, i - 1, j) + 1,
                                    valeur(matrice, i - 1, j - 1) + un_ou_zero(seq1, seq2, i - 1, j - 1)
                                    )

#################################
#### Calul de distance
#################################            

        
def calculer_distance_edition(seq1, seq2, matrice):
    """
    renvoie la distance d'édition entre les deux séquences en utilisant
    une matrice déjà remplie
    : param seq1, seq2 deux chaînes de caractères
    type str
    : param matrice, la matrice de mémoïsation remplie
    type list of list
    : return la distance
    type int
    >>> seq1 = 'aagtagccactag'
    >>> seq2 = 'aagtaagct'
    >>> matrice_1 = initialiser_matrice(seq1, seq2)
    >>> remplir_matrice(seq1, seq2, matrice_1)
    >>> calculer_distance_edition(seq1, seq2, matrice_1)
    6
    >>> seq3 = 'ALGORITHME'
    >>> seq4 = 'AGORRYTNE'
    >>> matrice_2 = initialiser_matrice(seq3, seq4)
    >>> remplir_matrice(seq3, seq4, matrice_2)
    >>> calculer_distance_edition(seq3, seq4, matrice_2)
    5
    """
    return valeur(matrice, len(seq1) - 1, len(seq2) - 1)

#####################################################
######## Alignement
#####################################################

def calculer_transformation(seq1, seq2):
    '''
    renvoie une chaîne de caractères permettant de visualiser les transformations
    à effectuer pour passer d'une sequence à l'autre.
    : param seq1, seq2
    type str
    : return
    type str
    >>> seq1 = 'aagtagccactag'
    >>> seq2 = 'aagta--agct--'
    >>> calculer_transformation(seq1, seq2)
    '|||||IIRR||II'
    >>> seq3 = 'ALGORITHME'
    >>> seq4 = 'A-GORRYTNE'
    >>> calculer_transformation(seq3, seq4)
    '|I|||RRRR|'
    '''
    transformation = ''
    for i in range(len(seq1)):
        if i < len(seq2) :
            if seq1[i] == seq2[i] :
                transformation += '|'
            elif seq1[i] == '-' :
                transformation += 'S'
            elif seq2[i] == '-' :
                transformation += 'I'
            else :
                transformation += 'R'
        else :
            transformation += 'S'
    return transformation


def inserer(seq, position):
    '''
    renvoie une sequence égale à la séquence passée en paramètre
    dans laquelle on a insérer un '-' à la position donnée
    : param seq
    type str
    : param position
    type int
    : return
    str
    '''
    return seq[:position] + '-' + seq[position:]

def alignement(seq1, seq2, matrice):
    """
    renvoie un tuple contenant les deux séquences alignées avec la ligne de transformation
    : param seq1, seq2
    type str
    : param matrice
    type list of liste
    : return
    Type tuple
    >>> seq1 = 'aagtagccactag'
    >>> seq2 = 'aagtaagct'
    >>> matrice_1 = initialiser_matrice(seq1, seq2)
    >>> remplir_matrice(seq1, seq2, matrice_1)
    >>> aligne = alignement(seq1, seq2, matrice_1)
    >>> aligne
    ('aagtagccactag', 'aagta--agct--', '|||||IIRR||II')
    >>> seq3 = 'ALGORITHME'
    >>> seq4 = 'AGORRYTNE'
    >>> matrice_2 = initialiser_matrice(seq3, seq4)
    >>> remplir_matrice(seq3, seq4, matrice_2)
    >>> aligne = alignement(seq3, seq4, matrice_2)
    >>> aligne
    ('ALGORITHME', 'A-GORRYTNE', '|I|||RRRR|')
    """
    i, j = len(seq1), len(seq2)
    while i > 0 and j > 0 :
        if (valeur(matrice, i, j) == valeur(matrice, i - 1, j - 1)
            and seq1[i - 1] == seq2[j - 1]
            ) :
            i, j = i - 1, j - 1            
        elif (valeur(matrice, i, j) == valeur(matrice, i - 1, j - 1) + 1 
              and seq1[i - 1] != seq2[j - 1]
              ) :
            i, j = i - 1, j - 1            
        elif valeur(matrice, i, j) == valeur(matrice, i, j - 1) + 1 :
            seq1 = inserer(seq1, i)
            j -= 1
        else :
            seq2 = inserer(seq2, j)
            i -= 1     
    seq1 = '-' * j + seq1
    seq2 = '-' * i + seq2
    return seq1, seq2, calculer_transformation(seq1, seq2)

##############################################
###### Comparaison genome
##############################################
    
def calculer_distance_genomes(nom_fichier_humain, nom_fichier_animal, affichage_console = False):
    '''
    renvoie un tuple distance entre les génomes ecrits dans les deux fichiers, longueur totale d'un génome
    Si affichage est Vrai, affiche les résultats au fur et à mesure dans la console
    : param nom_fichier_humain, nom_fichier_animal
    type str
    : param affichage_console
    type boolean
    : return la distance, longueur_totale_genome
    type tuple
    '''
    genome_animal = lecture('covid/' + nom_fichier_animal)
    genome_humain = lecture('covid/' + nom_fichier_humain)
    longueur_animal = len(genome_animal)
    longueur_humain = len(genome_humain)
    distance_totale = 0
    longueur_totale_seq = 0
    for i in range(0, min(longueur_animal, longueur_humain)):
        seq_humain = genome_humain[i]
        seq_animal = genome_animal[i]
        longueur_totale_seq += len(seq_humain)
        matrice = initialiser_matrice(seq_humain,seq_animal)
        remplir_matrice(seq_humain, seq_animal, matrice)
        distance_seq = calculer_distance_edition(seq_humain, seq_animal, matrice)
        distance_totale += distance_seq
        aligne = alignement(seq_humain, seq_animal, matrice)
        if affichage_console :
            afficher_donnees(aligne, distance_seq)
    return distance_totale, longueur_totale_seq



def test_covid():
    '''
    renvoie une liste contenant les résultats des tests entre les 3 humains et les animaux, MERS et SRAS
    : return
    type list
    '''
    resultats = [[0]* 3 for _ in range(4)]
    genome_covid = ['2019-nCoV_WH01.fa', '2019-nCoV_WH03.fa','2019-nCoV_WH04.fa']
    genome_compare = ['MERS-CoV.fa', 'SARS-CoV.fa', 'bat-SL-CoVZC45.fa', 'bat-SL-CoVZXC21.fa']
    for i in range(len(genome_covid)) :
        for j in range(len(genome_compare)):
            distance, longueur_seq = calculer_distance_genomes(genome_covid[i],genome_compare[j])
            resultats[j][i] = (distance, longueur_seq)
    return resultats

                                             
###################################################
########### Affichages dans la console
###################################################

def afficher_matrice(matrice):
    '''
    affiche la matrice dans la console (logueur max des valeurs de la matrice: 3 caractères)
    : param matrice
    type
    list of list
    '''
    for j in range(len(matrice)):
        print('-' * 41)
        for i in range(len(matrice[0])) :
            gauche = '|' + ' ' * ((3 - len(str(matrice[j][i]))) // 2)
            droite = ' ' * ( 3 - len(str(matrice[j][i])) - (3 - len(str(matrice[j][i]))) // 2)
            print(gauche + str(matrice[j][i]) + droite, end = '')
        print('|')
    print('-' * 41)

def afficher_donnees(aligne, distance):
    '''
    affiche les donnees dans la console
    : param seq_humain, seq_animal
    type str
    : param distance
    type int
    '''
    print(aligne[0])
    print(aligne[2], ' -> ', distance)
    print(aligne[1])
    print('\n')
    
def afficher_resultats(resultats, pourcentage = False):
    '''
    affiche le nombre de mutation si pourcentage vaut False (defaut) ou
    le % de compatibilité sinon
    : param resultats, une liste de résultats
    type list
    '''
    nom_covid = ['humain_1', 'humain_2', 'humain_4']
    nom_compare = ['  MERS  ', '  SARS  ', '  BAT45 ', '  BAT21 ']
    if pourcentage :
        print('% de compatibilité')
    else :
        print('nombre de transformations necessaires')
    print()
    for j in range(5):
        for i in range(4) :
            if j == 0 and i == 0 :
                print('         |', end= '')
            elif j == 0 and i > 0:
                print(nom_covid[i - 1]+ '|', end= '')
            elif i == 0 and j > 0 :
                print('|' + nom_compare[j - 1]+ '|', end= '')
            else :
                if pourcentage :
                    result = str(100 * (resultats[j - 1][i - 1][1] - resultats[j - 1][i - 1][0]) // resultats[j - 1][i - 1][1])+'%'
                else :
                    result = str(resultats[j - 1][i - 1][0])
                gauche = (8 - len(result)) // 2
                droite = 8 - len(result) - (8 - len(result)) // 2
                result = ' ' * gauche + result + ' ' * droite
                             
                print(result + '|', end= '')
        print('\n' + '-' * 37)






######################################"
######"" lancement doctest
######################################
    
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = False)