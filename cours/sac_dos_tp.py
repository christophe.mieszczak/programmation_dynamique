def interet(i,poids, valeurs, poids_restant):
    if i == -1 :
        return 0
    else :
        if poids_restant - poids[i] >= 0 :
            v1 = interet(i - 1, poids, valeurs, poids_restant - poids[i]) + valeurs[i]
            v2 = interet(i - 1, poids, valeurs, poids_restant)
            return max(v1, v2)
        else :
            return interet(i - 1, poids, valeurs, poids_restant)
    
# exercice 3 : une solution par programmation dynamique
# resultat de la matrice sur l'exemple
#       0   1   2   3   4   5   6   7   8   9  10  11
#-1 :   0   0   0   0   0   0   0   0   0   0   0   0
# 0 :   0   1   1   1   1   1   1   1   1   1   1   1
# 1 :   0   1   6   7   7   7   7   7   7   7   7   7
# 2 :   0   1   6   7   7  12  13  18  19  19  19  19
# 3 :   0   1   6   7   7  12  18  19  24  25  25  30
# 4 :   0   1   6   7   7  12  18  28  29  34  35  35




             
def progdyn(w,v,p):
    # décalage d'indice à cause du -1
    
    n = len(w)
    l = [ [ 0 for _ in range (0,p+1)] for _ in range(0,n+1) ]
    for i in range(1,n+1):
        for j in range (0,p+1):
            if j >= w[i-1]:                
                l[i][j] = max(l[i-1][j],l[i-1][j-w[i-1]]+v[i-1])
            else:
                l[i][j] = l[i-1][j]
    return l[n][p]

   


poids = [1,2,5,6,7]
valeurs = [1,6,12,18,28]
poids_max = 11
#valeur = interet(len(poids) - 1, poids, valeurs, poids_max)
valeur = interet(len(poids) - 1, poids, valeurs, poids_max)
print("Valeur du sac {}".format(valeur))
    
    
    