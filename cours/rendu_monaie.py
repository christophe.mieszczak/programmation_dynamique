from math import inf

def rendu_monnaie_mem(liste_pieces, somme):
    '''
    construit un tableau de longueur somme + 1 
    '''
    mem = [0] * (somme + 1)
    return rendu_monnaie_mem_c(liste_pieces, somme, mem)
                 
                 
def rendu_monnaie_mem_c(liste_pieces, somme, m):
    '''
    renvoie le nbre de pièces minià utiliser pour rembourser
    la somme en utilisant les pièces de la liste_pieces
    : param liste_pieces
    type list
    : param somme
    type int
     : param m pour mémoriser les calculs déjà fait
     type list
    '''
    for somme_a_rendre in range(1, somme + 1):
        m[somme_a_rendre] = inf
        for j in range(len(liste_pieces)):
            print(liste_pieces[j] , '/', m)
            if liste_pieces[j] <= somme_a_rendre :
                m[somme_a_rendre] = min( m[somme_a_rendre], 1+ m[somme_a_rendre - liste_pieces[j]])
    return m[somme]

