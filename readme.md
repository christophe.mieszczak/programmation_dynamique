# TP CoVID



## Remarques

* Je n'ai pas vu le squelette de suite et ne l'ai donc pas tout à fait suivi :
    * J'ai renommé quelques fonctions et variables de façon à les rendre plus parlantes et à suivre les préconisations de nommage en Python.
    * la fonction `initialiser_matrice` renvoie une matrice où la première ligne et la première colonne sont déjà complétées.
    * la fonction `remplir_matrice`,  qui correspond à `MdistanceEdition` , ne renvoie pas une nouvelle matrice mais utilise un effet de bord sur la matrice passée en paramètre, présent mais inutilisé dans `MdistanceEdition`. 
    * Les DOCSTRINGS sont complétées.
    * La fonction `test` est inutile car tous les tests sont effectués dans les DOCTESTS.
    * Les fonctions d'affichage sont séparées des autres (pas de `print` hors de ces fonctions).

## Aperçus

```python
>>> seq1 = 'AGORRYTNE'
>>> seq2 = 'ALGORITHME'
>>> matrice = initialiser_matrice(seq1, seq2)

-----------------------------------------
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
-----------------------------------------
| 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 2 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 3 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 4 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 5 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 6 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 7 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 8 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
| 9 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------
|10 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
-----------------------------------------

>>> remplir_matrice(seq1, seq2, matrice)
>>> afficher_matrice(matrice)
-----------------------------------------
| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
-----------------------------------------
| 1 | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
-----------------------------------------
| 2 | 1 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
-----------------------------------------
| 3 | 2 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
-----------------------------------------
| 4 | 3 | 2 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |
-----------------------------------------
| 5 | 4 | 3 | 2 | 1 | 2 | 3 | 4 | 5 | 6 |
-----------------------------------------
| 6 | 5 | 4 | 3 | 2 | 2 | 3 | 4 | 5 | 6 |
-----------------------------------------
| 7 | 6 | 5 | 4 | 3 | 3 | 3 | 3 | 4 | 5 |
-----------------------------------------
| 8 | 7 | 6 | 5 | 4 | 4 | 4 | 4 | 4 | 5 |
-----------------------------------------
| 9 | 8 | 7 | 6 | 5 | 5 | 5 | 5 | 5 | 5 |
-----------------------------------------
|10 | 9 | 8 | 7 | 6 | 6 | 6 | 6 | 6 | 5 |
-----------------------------------------

>>> aligne = alignement(seq1, seq2, matrice)
>>> distance = calculer_distance_edition(seq1, seq2, matrice)
>>> afficher_donnees(aligne, distance)
A-GORRYTNE
|S|||RRRR|  ->  5
ALGORITHME

>>> calculer_distance_genomes('2019-nCoV_WH04.fa', 'bat-SL-CoVZC45.fa', True)
------------T---TTCCCAGGTAACAAACCAACCAACTTTCGATCTCTTGTAGATCTGTTCTCTAAACGAAC
SSSSSSSSSSSS|SSS||||||||||||||||||||R||||R|||||||||||||||IIIIIII|||IIIIIIII  ->  32
ATATTAGGTTTTTACCTTCCCAGGTAACAAACCAACTAACTCTCGATCTCTTGTAGA-------TCT--------


--T-T-TAAA--ATCTGT-GTGGCTGTCACTCGGCTG-CATGCTTAG-TGCACTCACGCAGTATAATTA
SS|S|S||||SS|R||R|SRRRR||||IIR|I|R|||S||II|||||S||||I|IR|RR|||IRR|R|I  ->  32
GTTCTCTAAACGAACTTTAAAATCTGT--GT-GACTGTCA--CTTAGCTGCA-T-GCTTAGT-GCACT-


--------------ATAACTAATTACTGTCGTTGACAGGACACGAGTAACTCGTCTATCTTCTGCAGGCTGCTT
SSSSSSSSSSSSSS||||R||||||||||||||||||||||||||||||||||IIIIIII|||II|III|I|I|  ->  29
CACGCAGTTTAATTATAATTAATTACTGTCGTTGACAGGACACGAGTAACTCG-------TCT--A---T-C-T

[.............etc .....................]

ACCAGTTGTTCAGACTATTGAAGTGAATAGTTTTAGTGGTTATTTAAAACTTACTGACAA-
IRRR|||RR|R|R||||R||||RRR||||R|||R|RR||||||||||||R|R||||||||S  ->  19
-GTTGTTACTGAAACTAGTGAACAAAATAATTTCACAGGTTATTTAAAATTAACTGACAAT


TGTATACATTAAAAATGCAGACATTGTGGAAGAAGCTAAAAAGGTAAAACCAACAGT-GGT
I||R|R||||||||||||R||||||||R||||||||||||||||||||R||R|||||S|R|  ->  9
-GTCTTCATTAAAAATGCTGACATTGTAGAAGAAGCTAAAAAGGTAAAGCCTACAGTAGTT


TGTTAATGCAGCCAATGTTTACCTTAAACATGGAGGAGGTGTTGCAGGAGCCTTAAATAA-
I|||||||||||R||||||||||||||||||||||||||||||||R|||||R||||||||S  ->  5
-GTTAATGCAGCTAATGTTTACCTTAAACATGGAGGAGGTGTTGCTGGAGCTTTAAATAAG


GGCTACTAACAATGCCATGCAAGTTGAATCTGATGATTACATAGCTACTAATGGACCACT-
I||R||||||||R||||||||R||||||||||||R|R||||||R||||R|||||R|||||S  ->  10
-GCAACTAACAACGCCATGCAGGTTGAATCTGATAAGTACATAACTACCAATGGGCCACTA


[.............etc .....................]

AGGGAGAGCTGCCTATATGGAAGAGCCCTAATGTGTAAAATTAATTTTAGTAGTGCTATC
IIRR|I|IIII||I|IR|IIIR||III|I||I|IRR||||RR||RRRR|RR|RRRRR|RR  ->  38
--TCA-A----CC-A-CT---CGA---C-AA-G-AAAAAAAAAAAAAAAAAAAAAAAAAA

(9099, 29820)

>>> resultats = test_covid()
>>> afficher_resultats(resultats)
nombre de transformations necessaires

         |humain_1|humain_2|humain_4|
-------------------------------------
|  MERS  | 15430  | 16102  | 16330  |
-------------------------------------
|  SARS  | 16300  | 16245  | 16163  |
-------------------------------------
|  BAT45 | 14690  | 11998  |  9099  |
-------------------------------------
|  BAT21 | 16175  | 16331  | 16144  |
-------------------------------------
>>> afficher_resultats(resultats, True)
% de compatibilité

         |humain_1|humain_2|humain_4|
-------------------------------------
|  MERS  |  48%   |  46%   |  45%   |
-------------------------------------
|  SARS  |  45%   |  45%   |  45%   |
-------------------------------------
|  BAT45 |  50%   |  59%   |  69%   |
-------------------------------------
|  BAT21 |  45%   |  45%   |  45%   |
-------------------------------------

```



Il semblerait que le proche virus soit BAT45. Je suis tout de même étonné du nombre important de mutations.